Pod::Spec.new do |s|

  s.name         = "AppBrillianceApi"
  s.version      = "0.0.36"
  s.summary      = "AppBrilliance API."
  s.description  = <<-DESC
  A This is the AppBrilliance API
                   DESC

  s.homepage     = "https://gitlab.com/andoni.arostegi/AppBrillianceApiDist/raw/master/AppBrillianceApi-0.0.36.zip"
  s.license      = { :type => "MIT", :file => "license" }
  s.author             = { "Andoni Arostegui" => "andoni.arostegi@gmail.com" }
  s.ios.deployment_target = '9.3'
  s.ios.vendored_frameworks = 'AppBrillianceApi.framework', 'SwiftyJSON.framework'
  s.source            = { :http => 'https://gitlab.com/andoni.arostegi/AppBrillianceApiDist/raw/master/AppBrillianceApi-0.0.36.zip' }
  s.exclude_files = "Classes/Exclude"

end
